class Rectangulo:
    def __init__ (self, b, h):
        self.base = b
        self.altura = h

    def calcula_area (self):
        calcula_area = self.base*self.altura
        return calcula_area

    def calcula_perimetro (self):
        calcula_perimetro = 2*(self.base+self.altura)
        return calcula_perimetro

    def __str__ (self):
        return "El área del rectángulo es de {area:.2f} m2 y su perimetro es de {perimetro:.2f} m".format(area = self.calcula_area(), perimetro = self.calcula_perimetro())

"""base = float(input("Ingresa la base del rectángulo en metros: "))
altura = float(input("Ingresa la altura del rectángulo en metros: "))
rectangulo1 = Rectangulo(base, altura)

print(rectangulo1)"""

rectangulo1 = Rectangulo(4, 6)

print(rectangulo1)