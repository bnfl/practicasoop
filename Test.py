import unittest
from Fraccion import Fraccion
from Cuadrado import Cuadrado
from Rectangulo import Rectangulo

class TestFraccion(unittest.TestCase):
    def test_suma(self):
        fracciones1 = Fraccion(2, 5)
        self.assertEqual(fracciones1.suma_fracciones(), 7)
    
    def test_resta(self):
        fracciones1 = Fraccion(2, 5)
        self.assertEqual(fracciones1.resta_fracciones(), -3)

    def test_multiplica(self):
        fracciones1 = Fraccion(2, 5)
        self.assertEqual(fracciones1.multiplica_fracciones(), 10)

    def test_divide(self):
        fracciones1 = Fraccion(2, 5)
        self.assertAlmostEqual(fracciones1.divide_fracciones(), 0.4)

class TestCuadrado(unittest.TestCase):
    def test_calcula_area_con_valores_correctos(self): 
        cuadradoL = Cuadrado(6)
        area1 = cuadradoL.calcula_area()
        self.assertEqual(area1, 36)

    def test_calcula_area_con_valores_incorrectos(self): 
        cuadradoL = Cuadrado(6)
        area1 = cuadradoL.calcula_area()
        self.assertNotEqual(area1, 25)

    def test_calcula_perimetro_con_valores_correctos(self): 
        cuadradoL = Cuadrado(6)
        perimetro1 = cuadradoL.calcula_perimetro()
        self.assertEqual(perimetro1, 24)

    def test_calcula_perimetro_con_valores_incorrectos(self): 
        cuadradoL = Cuadrado(6)
        perimetro1 = cuadradoL.calcula_perimetro()
        self.assertNotEqual(perimetro1, 30)             

class TestRectangulo(unittest.TestCase):
    def test_calcula_area_con_valores_correctos (self):
        rectangulo1 = Rectangulo(4, 6)
        self.assertEqual(rectangulo1.calcula_area(), 24)
    
    def test_calcula_area_con_valores_incorrectos (self):
        rectangulo1 = Rectangulo(4, 6)
        self.assertNotEqual(rectangulo1.calcula_area(), 30)

    def test_calcula_perimetro_con_valores_correctos (self):
        rectangulo1 = Rectangulo(4, 6)
        self.assertEqual(rectangulo1.calcula_perimetro(), 20)

    def test_calcula_perimetro_con_valores_incorrectos (self):
        rectangulo1 = Rectangulo(4, 6)
        self.assertNotEqual(rectangulo1.calcula_perimetro(), 25)

if __name__ == '__main__':
    unittest.main()