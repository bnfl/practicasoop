class Cuadrado:

	def __init__(self, l):
		self.lado=l
	
	def calcula_area (self):
		return self.lado**2
		
	def calcula_perimetro(self):
		return self.lado*4	
		
	def __str__ (self):
		return "El área del cuadrado es {area:.2f}m² y su perímetro es {perimetro}m". format(area = self.calcula_area(), perimetro = self.calcula_perimetro())

#lado = int(input("Introduzca el lado del cuadrado en metros: "))

cuadradoL = Cuadrado(6)

print(cuadradoL)