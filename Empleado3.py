class Empleado:
    lista_empleados = {}

    def __init__(self, n, s, g):
        self.nombre = n
        self.salario = s
        self.genero = g
        Empleado.lista_empleados[self.nombre] = self

    def calcular_impuestos(self):
        return self.salario * 0.30

    def __str__(self):
        if self.genero == "masculino":
            return "El empleado {} tiene un salario de {:.2f} euros y la cantidad de impuestos a pagar es {:.2f} euros".format(self.nombre, self.salario, self.calcular_impuestos())
        elif self.genero == "femenino":
            return "La empleada {} tiene un salario de {:.2f} euros y la cantidad de impuestos a pagar es {:.2f} euros".format(self.nombre, self.salario, self.calcular_impuestos())
        else:
            return "El empleado {} tiene un salario de {:.2f} euros y la cantidad de impuestos a pagar es {:.2f} euros".format(self.nombre, self.salario, self.calcular_impuestos())

empleadoPepe = Empleado("Pepe", 20000, "masculino")
empleadoAna = Empleado("Ana", 30000, "femenino")
empleadoManuel = Empleado("Manuel", 25000, "masculino")
empleadoMaria = Empleado("Maria", 35000, "femenino")

"""nombre = input("Introduzca el nombre del empleado: ")

if nombre in Empleado.lista_empleados:
    print(Empleado.lista_empleados[nombre])
else:
    print("El empleado no está en la lista.")"""

print(empleadoPepe)
    