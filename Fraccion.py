import unittest

class Fraccion:
	
	def __init__ (self, a, b):
		self.fraccion1 = a
		self.fraccion2 = b
	
	def suma_fracciones (self):
		suma_fracciones = self.fraccion1 + self.fraccion2
		return suma_fracciones
	
	def resta_fracciones (self):
		resta_fracciones = self.fraccion1 - self.fraccion2
		return resta_fracciones
		
	def multiplica_fracciones (self):
		multiplica_fracciones = self.fraccion1 * self.fraccion2
		return multiplica_fracciones
		
	def divide_fracciones (self):
		divide_fracciones = self.fraccion1 / self.fraccion2
		return divide_fracciones
	
	def __str__ (self):
		return "La suma de las fracciones es {suma:.2f}, su resta es {resta:.2f}, su producto es {producto:.2f} y su división es {division:.2f}".format(suma = self.suma_fracciones(), resta = self.resta_fracciones(), producto = self.multiplica_fracciones(), division = self.divide_fracciones())
		
fraccion1 = Fraccion(1/2, 3/2) 

print(fraccion1)
